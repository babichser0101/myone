/*=========================================================================================
    File Name: app-todo.js
    Description: app-todo
    ----------------------------------------------------------------------------------------
    Item Name: Frest HTML Admin Template
   Version: 1.0
    Author: PIXINVENT
    Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/
// Todo App variables
$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});
var todoNewTasksidebar = $(".todo-new-task-sidebar"),
  appContentOverlay = $(".app-content-overlay"),
  sideBarLeft = $(".sidebar-left"),
  todoTaskListWrapper = $(".todo-task-list-wrapper"),
  todoItem = $(".todo-item"),
  selectAssignLable = $(".select2-assign-label"),
  selectUsersName = $(".select2-users-name"),
  avatarUserImage = $(".avatar-user-image"),
  updateTodo = $(".update-todo"),
  addTodo = $(".add-todo"),
  // markCompleteBtn = $(".mark-complete-btn"),
  editTaskTitle = $(".edit-task-title"),
  newTaskTitle = $(".new-task-title"),
  taskTitle = $(".task-title"),
  noResults = $(".no-results"),
  assignedAvatarContent = $(".assigned .avatar .avatar-content"),
  todoAppMenu = $(".todo-app-menu");

// badge colors object define here for badge color
var badgeColors = {
  "Frontend": "badge-light-primary",
  "Backend": "badge-light-success",
  "Issue": "badge-light-danger",
  "Design": "badge-light-warning",
  "Wireframe": "badge-light-info",
}

$(function () {
  "use strict";

  // Single Date Picker
  $('.pickadate').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    locale: {
      format: 'MM/DD/YY'
    }
  });

  // dragable list
  dragula([document.getElementById("todo-task-list-drag")], {
    moves: function (el, container, handle) {
      return handle.classList.contains("handle");
    }
  });


  // select assigner
  selectUsersName.select2({
    placeholder: "Unassigned",
    dropdownAutoWidth: true,
    width: '100%'
  });

  // select label
  selectAssignLable.select2({
    dropdownAutoWidth: true,
    width: '100%'
  });

  // Sidebar scrollbar
  if ($('.todo-application .sidebar-menu-list').length > 0) {
    var sidebarMenuList = new PerfectScrollbar('.sidebar-menu-list', {
      theme: "dark",
      wheelPropagation: false
    });
  }

  //  New task scrollbar
  if (todoNewTasksidebar.length > 0) {
    var todo_new_task_sidebar = new PerfectScrollbar('.todo-new-task-sidebar', {
      theme: "dark",
      wheelPropagation: false
    });
  }

  // Task list scrollbar
  if ($('.todo-application .todo-task-list').length > 0) {
    var sidebar_todo = new PerfectScrollbar('.todo-task-list', {
      theme: "dark",
      wheelPropagation: false
    });
  }

 

  // **************Sidebar Left**************//
  // -----------------------------------------

  // Main menu toggle should hide app menu
  $('.menu-toggle').on('click', function () {
    sideBarLeft.removeClass('show');
    appContentOverlay.removeClass('show');
    todoNewTasksidebar.removeClass('show');
  });

  //on click of app overlay removeclass show from sidebar left and overlay
  appContentOverlay.on('click', function () {
    sideBarLeft.removeClass('show');
    appContentOverlay.removeClass('show');
  });

  // Add class active on click of sidebar menu's filters
  todoAppMenu.find(".list-group a").on('click', function () {
    var $this = $(this);
    todoAppMenu.find(".active").removeClass('active');
    $this.addClass("active")
    // if active class available icon color primary blue else gray
    if ($this.hasClass('active')) {
      $this.find(".livicon-evo").updateLiviconEvo({
        strokeColor: '#5A8DEE'
      });
      todoAppMenu.find(".list-group a").not(".active").find(".livicon-evo").updateLiviconEvo({
        strokeColor: '#475f7b'
      });
    }
  });

  //On compose btn click of compose mail visible and sidebar left hide
  $('.add-task-btn').on('click', function () {
    //show class add on new task sidebar,overlay
    todoNewTasksidebar.addClass('show');
    appContentOverlay.addClass('show');
    sideBarLeft.removeClass('show');
    taskTitle.focus();
    //d-none add on avatar and remove from avatar-content
    avatarUserImage.addClass("d-none");
    assignedAvatarContent.removeClass("d-none");
    //select2 value null assign
    selectUsersName.val(null).trigger('change');
    selectAssignLable.val(null).trigger('change');
    //update button has add class d-none remove from add TODO
    updateTodo.addClass("d-none");
    addTodo.removeClass("d-none");
    //mark complete btn should hide & new task title will visible
    editTaskTitle.addClass("d-none");
    newTaskTitle.removeClass("d-none");
    //Input field Value empty
    taskTitle.val("");
    selectAssignLable.attr("disabled", "true");
  });

  // On sidebar close click hide sidebarleft and overlay
  $(".todo-application .sidebar-close-icon").on('click', function () {
    sideBarLeft.removeClass('show');
    appContentOverlay.removeClass('show');
  });

  // **************New Task sidebar**************//
  // ---------------------------------------------

  // add new task
  addTodo.on("click", function () {
   if (taskTitle.val().length > 0) {

      $.post("/supplier/addNew_RoomType",
        {
          title: taskTitle.val(),
        },
        function(data, ){
          var titleTask = taskTitle.val();

          todoTaskListWrapper.append(
            // append a new task in task list
            '<li class="todo-item no-animation" data-status="0" data-name="' + data.message + '">' +
            '<div class="todo-title-wrapper d-flex justify-content-between align-items-center">' +
            '<div class="todo-title-area d-flex">' +
            '<i class="bx bx-grid-vertical handle"></i><p class="todo-title mx-50 m-0 truncate">' + titleTask + '</p>' +
            '</div>' +
            '<div class="todo-item-action d-flex align-items-center"><div class="todo-badge-wrapper d-flex"><span class="badge badge-light-danger badge-pill" id="status_span'+data.message+'">inactive</span></div><a class="todo-item-favorite ml-50 warning" data-id="'+data.message+'">' +
            '<i class="bx bx-star"></i><span>active</span>' + '</a>' +
            '<a class="todo-item-block ml-50" data-id="'+data.message+'">' + '<i class="bx bx-trash"></i><span>block</span>' + '</a>' +
            '</div></div></li>');
          // new task sidebar, overlay hide
          todoNewTasksidebar.removeClass('show');
          appContentOverlay.removeClass('show');
          selectAssignLable.attr("disabled", "true");
        });

      

    }
    else {
      // new task sidebar, overlay hide
      todoNewTasksidebar.removeClass('show');
      appContentOverlay.removeClass('show');
      selectAssignLable.attr("disabled", "true");
    }
  });

  // On Click of Close Icon btn, cancel btn and overlay remove show class from new task sidebar and overlay
  // and reset all form fields
  $(".close-icon, .cancel-btn, .app-content-overlay, .mark-complete-btn").on('click', function () {
    todoNewTasksidebar.removeClass('show');
    appContentOverlay.removeClass('show');
    setTimeout(function () {
      todoNewTasksidebar.find('textarea').val("");
      selectAssignLable.attr("disabled", "true");
    }, 100)
  });

  // on click of add label icon select 2 display
  $(".add-tags").on("click", function () {
    if (selectAssignLable.is('[disabled]')) {
      selectAssignLable.removeAttr("disabled");
    }
    else {
      selectAssignLable.attr("disabled", "true");
    }
  });

  // Update Task
  updateTodo.on("click", function () {
    todoNewTasksidebar.removeClass('show');
    appContentOverlay.removeClass('show');
    selectAssignLable.attr("disabled", "true");
  });

  // ************Rightside content************//
  // -----------------------------------------

  // Search filter for task list
  $(document).on("keyup", ".todo-search", function () {
    todoItem = $(".todo-item");
    $('.todo-item').css('animation', 'none')
    var value = $(this).val().toLowerCase();
    if (value != "") {
      todoItem.filter(function () {
        $(this).toggle($(this).text().toLowerCase().includes(value));
      });
      var tbl_row = $(".todo-item:visible").length; //here tbl_test is table name

      //Check if table has row or not
      if (tbl_row == 0) {
        if (!noResults.hasClass('show')) {
          noResults.addClass('show');
        }
      }
      else {
        noResults.removeClass('show');
      }
    }
    else {
      // If filter box is empty
      todoItem.show();
      if (noResults.hasClass('show')) {
        noResults.removeClass('show');
      }
    }
  });
  // on Todo Item click show data in sidebar
  var globalThis = ""; // Global variable use in multiple function
  todoTaskListWrapper.on('click', '.todo-item', function (e) {
    var $this = $(this);
    globalThis = $this;

    todoNewTasksidebar.addClass('show');
    appContentOverlay.addClass('show');

    var todoTitle = $this.find(".todo-title").text();
    taskTitle.val(todoTitle);
    
    // if avatar is available
    if ($this.find(".avatar img").length) {
      avatarUserImage.removeClass("d-none");
      assignedAvatarContent.addClass("d-none");
    }
    else {
      avatarUserImage.addClass("d-none");
      assignedAvatarContent.removeClass("d-none");
    }
    //current task's image source assign to variable
    var avatarSrc = $this.find(".avatar img").attr('src');

    avatarUserImage.attr("src", avatarSrc);
    var assignName = $this.attr('data-name');

    $(".select2-users-name").val(assignName).trigger('change');

    // badge selected value check
    if ($(this).find('.badge').length) {
      //if badge available in current task
      var badgevalAll = [];
      var selected = $(this).find('.badge');

      selected.each(function () {
        var badgeVal = $(this).text();
        badgevalAll.push(badgeVal);
        selectAssignLable.val(badgevalAll).trigger("change");
      });
    }
    else {
      selectAssignLable.val(null).trigger("change");
    }
    // update button has remove class d-none & add class d-none in add todo button
    updateTodo.removeClass("d-none");
    addTodo.addClass("d-none");
    editTaskTitle.removeClass('d-none');
    newTaskTitle.addClass("d-none");

  }).on('click', '.todo-item-favorite', function (e) {
    e.stopPropagation();
    var tongle_button = $(this);
    $.ajax({
      url: '/supplier/updateStatus_roomType/'+  $(this).data('id')+'?active='+$("#roomType_"+$(this).data('id')).data('status'),
      dataType: "json",
      contentType: "application/json"
    }).done(function(response) {
      // alert($(this).data('id'));
      // $('.todo-item-favorite').toggleClass("warning");
      $("#roomType_"+tongle_button.data('id')).data('status', response.message);
      tongle_button.toggleClass("warning");
      tongle_button.toggleClass("danger");
      tongle_button.find('span').html("");
      
      if(response.message == '1'){
        // alert(response.message)
        tongle_button.find('span').html("inactive")
        
        $("#status_span"+tongle_button.data('id')).removeClass('badge-light-danger')
        $("#status_span"+tongle_button.data('id')).removeClass('badge-light-secondary')
        $("#status_span"+tongle_button.data('id')).addClass('badge-light-primary')
        $("#status_span"+tongle_button.data('id')).html("")
        $("#status_span"+tongle_button.data('id')).html("active")
        $("#blockBtn_"+tongle_button.data('id')).removeClass('danger')
        $("#blockBtn_"+tongle_button.data('id')).find('span').html("")
        $("#blockBtn_"+tongle_button.data('id')).find('span').html("block")
      }else{
        tongle_button.find('span').html("active")
        
        $("#status_span"+tongle_button.data('id')).removeClass('badge-light-primary')
        $("#status_span"+tongle_button.data('id')).removeClass('badge-light-secondary')
        $("#status_span"+tongle_button.data('id')).addClass('badge-light-danger')
        $("#status_span"+tongle_button.data('id')).html("")
        $("#status_span"+tongle_button.data('id')).html("inactive")
        $("#blockBtn_"+tongle_button.data('id')).removeClass('danger')
        $("#blockBtn_"+tongle_button.data('id')).find('span').html("")
        $("#blockBtn_"+tongle_button.data('id')).find('span').html("block")
      }
      
      tongle_button.find("i").toggleClass("bxs-star");
    });
  }).on('click', '.todo-item-block', function (e) {
    e.stopPropagation();
    var block_btn = $(this);
    $.ajax({
      url: '/supplier/updateblock_roomType/'+  $(this).data('id'),
      dataType: "json",
      contentType: "application/json"
    }).done(function(response) {
      console.log(response);
      block_btn.addClass('danger');
      $("#status_span"+block_btn.data('id')).removeClass('badge-light-danger')
      $("#status_span"+block_btn.data('id')).removeClass('badge-light-primary')
      $("#status_span"+block_btn.data('id')).addClass('badge-light-secondary')
      $("#status_span"+block_btn.data('id')).html("")
      $("#status_span"+block_btn.data('id')).html("blocked")
      $("#activeBtn_"+block_btn.data('id')).removeClass("danger")
      $("#activeBtn_"+block_btn.data('id')).addClass("warning")
      $("#activeBtn_"+block_btn.data('id')).find('span').html("")
      $("#activeBtn_"+block_btn.data('id')).find('span').html("active")
      block_btn.find('span').html("")
      block_btn.find('span').html("blocked")
    })
    // $(this).closest('.todo-item').remove();
  }).on('click', '.checkbox', function (e) {
    e.stopPropagation();
  });

  // Complete task strike through
  todoTaskListWrapper.on('click', ".todo-item .checkbox-input", function (e) {
    $(this).closest('.todo-item').toggleClass("completed");
  });


  // Todo sidebar toggle
  $('.sidebar-toggle').on('click', function (e) {
    e.stopPropagation();
    sideBarLeft.toggleClass('show');
    appContentOverlay.addClass('show');
  });

  // sorting task list item
  $(".ascending").on("click", function () {
    todoItem = $(".todo-item");
    $('.todo-item').css('animation', 'none')
    todoItem.sort(sort_li).appendTo(todoTaskListWrapper);
    function sort_li(a, b) {
      return ($(b).find('.todo-title').text().toLowerCase()) < ($(a).find('.todo-title').text().toLowerCase()) ? 1 : -1;
    }
  });

  // descending sorting
  $(".descending").on("click", function () {
    todoItem = $(".todo-item");
    $('.todo-item').css('animation', 'none')
    todoItem.sort(sort_li).appendTo(todoTaskListWrapper);
    function sort_li(a, b) {
      return ($(b).find('.todo-title').text().toLowerCase()) > ($(a).find('.todo-title').text().toLowerCase()) ? 1 : -1;
    }
  });
});

$(window).on("resize", function () {
  // remove show classes from sidebar and overlay if size is > 992
  if ($(window).width() > 992) {
    if (appContentOverlay.hasClass('show')) {
      sideBarLeft.removeClass('show');
      appContentOverlay.removeClass('show');
      todoNewTasksidebar.removeClass("show");
    }
  }
});

