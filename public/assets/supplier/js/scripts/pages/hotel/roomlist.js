/*=========================================================================================
    File Name: page-users.js
    Description: Users page
    --------------------------------------------------------------------------------------
    Item Name: Frest HTML Admin Template
    Version: 1.0
    Author: PIXINVENT
    Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/
$.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
$(document).ready(function () {

    $(".search_hotelcode").select2({
        dropdownAutoWidth: true,
        width: '100%'
    });
    // variable declaration
    var usersTable;
    var usersDataArray = [];
    // datatable initialization
    if ($("#room-list-datatable").length > 0) {
        usersTable = $("#room-list-datatable").DataTable({
            // searching: false,
            responsive: true,
            'columnDefs': [
                {
                    "orderable": false,
                    "targets": [7]
                }]
        });
    };
    // on click selected users data from table(page named page-users-list)
    // to store into local storage to get rendered on second page named page-users-view
    $(document).on("click", "#room-list-datatable tr", function () {
        $(this).find("td").each(function () {
            usersDataArray.push($(this).text().trim())
        })
        localStorage.setItem("usersId", usersDataArray[0]);
        localStorage.setItem("usersUsername", usersDataArray[1]);
        localStorage.setItem("usersName", usersDataArray[2]);
        localStorage.setItem("usersVerified", usersDataArray[4]);
        localStorage.setItem("usersRole", usersDataArray[5]);
        localStorage.setItem("usersStatus", usersDataArray[6]);
    })
    // render stored local storage data on page named page-users-view
    if (localStorage.usersId !== undefined) {
        $(".room-view-id").html(localStorage.getItem("usersId"));
        $(".room-view-username").html(localStorage.getItem("usersUsername"));
        $(".room-view-name").html(localStorage.getItem("usersName"));
        $(".room-view-verified").html(localStorage.getItem("usersVerified"));
        $(".room-view-role").html(localStorage.getItem("usersRole"));
        $(".room-view-status").html(localStorage.getItem("usersStatus"));
        // update badge color on status change
        if ($(".room-view-status").text() === "Banned") {
            $(".room-view-status").toggleClass("badge-light-success badge-light-danger")
        }
        // update badge color on status change
        if ($(".room-view-status").text() === "Close") {
            $(".room-view-status").toggleClass("badge-light-success badge-light-warning")
        }
    }
    // page users list verified filter
    $("#room-list-verified").on("change", function () {
        var usersVerifiedSelect = $("#room-list-verified").val();
        usersTable.search(usersVerifiedSelect).draw();
    });
    // page users list role filter
    $("#room-list-role").on("change", function () {
        var usersRoleSelect = $("#room-list-role").val();
        // console.log(usersRoleSelect);
        usersTable.search(usersRoleSelect).draw();
    });
    // page users list status filter
    $("#room-list-status").on("change", function () {
        var usersStatusSelect = $("#room-list-status").val();
        // console.log(usersStatusSelect);
        usersTable.search(usersStatusSelect).draw();
    });
    // users language select
    if ($("#room-language-select2").length > 0) {
        $("#room-language-select2").select2({
            dropdownAutoWidth: true,
            width: '100%'
        });
    }
    // page users list clear filter
    $(".room-list-clear").on("click", function(){
        usersTable.search("").draw();
    })
    // users music select
    if ($("#room-music-select2").length > 0) {
        $("#room-music-select2").select2({
            dropdownAutoWidth: true,
            width: '100%'
        });
    }
    // users movies select
    if ($("#room-movies-select2").length > 0) {
        $("#room-movies-select2").select2({
            dropdownAutoWidth: true,
            width: '100%'
        });
    }
    // users birthdate date
    if ($(".birthdate-picker").length > 0) {
        $('.birthdate-picker').pickadate({
            format: 'mmmm, d, yyyy'
        });
    }
    // Input, Select, Textarea validations except submit button validation initialization
    if ($(".room-edit").length > 0) {
        $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
    }

    $(".room_activate").on('click', function(){
        var this_object = $(this);
        $.ajax({
            type: 'post',
            url: '/supplier/updateRoomStatus/'+$(this).data('id'),
            success: function(data) {
                console.log(data);
                this_object.toggleClass('btn-success');
                this_object.toggleClass('btn-danger');
                $(".status_alert"+this_object.data('id')).toggleClass('badge-light-danger');
                $(".status_alert"+this_object.data('id')).toggleClass('badge-light-success');
                $(".status_alert"+this_object.data('id')).html('');
                if(data.message==true){
                    $(".status_alert"+this_object.data('id')).html("inactive");
                    this_object.find('span').html("inactive")
                }else{
                    $(".status_alert"+this_object.data('id')).html("inactive");
                    this_object.find('span').html("active")
                }
                this_object.find('i').toggleClass('bx-time');
                this_object.find('i').toggleClass('bx-bug');
            }

        })

        
    })
});
