/*=========================================================================================
    File Name: wizard-steps.js
    Description: wizard steps page specific js
    ----------------------------------------------------------------------------------------
    Item Name: Frest HTML Admin Template
    Version: 1.0
    Author: PIXINVENT
    Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/
//    Wizard tabs with icons setup
// ------------------------------
$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

var address = "Las Vegas, NV";
var addhotel_wizard = $(".wizard-horizontal");
var addhotel_form = addhotel_wizard.show();
addhotel_wizard.steps({
  headerTag: "h6",
  bodyTag: "fieldset",
  transitionEffect: "fade",
  titleTemplate: '<span class="step">#index#</span> #title#',
  labels: {
    finish: 'Submit'
  },
  onStepChanging: function (event, currentIndex, newIndex) {
      console.log(currentIndex + ":::" + newIndex);
      if(newIndex == 1){
        address = $("#cityName").val();
        codeAddress(address);
        
        // codeAddress(geocoder, map, address)
      }
    // Allways allow previous action even if the current form is not valid!
    if (currentIndex > newIndex) {
      return true;
    }
    addhotel_form.validate().settings.ignore = ":disabled,:hidden";
    return addhotel_form.valid();
  },
  onFinished: function (event, currentIndex) {
    console.log(addhotel_form.serialize())
    $.ajax({
      type: 'post',
      url: '/supplier/hotel_newInsert',
      data: addhotel_form.serialize(),
      dataType: 'json',
      success: function(data) {
        console.log(data)
        if(data.result=='success'){
          window.location.href = "/hotel-list";
        }
      }
    });
   
  },
});

addhotel_wizard.validate({
  ignore: 'input[type=hidden]', // ignore hidden fields
  errorClass: 'danger',
  successClass: 'success',
  highlight: function (element, errorClass) {
    $(element).removeClass(errorClass);
  },
  unhighlight: function (element, errorClass) {
    $(element).removeClass(errorClass);
  },
  errorPlacement: function (error, element) {
    error.insertAfter(element);
  },
  rules: {
    hotel_property_name: {
      required: true,
    },
    hotel_star_rating:{
      required: true,
    },
    currency_type:{
      required: true,
    },
    cityName:{
      required: true,
    },
    email: {
      required: true,
      email: true
    }
  },
  messages:{
    hotel_property_name: "name is requried",
    hotel_star_rating: "please put the rating",
    cityName: "please put the cityname"
  }
});

//       Validate steps wizard //
// -----------------------------
// Show form
var stepsValidation = $(".wizard-validation");
var form = stepsValidation.show();

stepsValidation.steps({
  headerTag: "h6",
  bodyTag: "fieldset",
  transitionEffect: "fade",
  titleTemplate: '<span class="step">#index#</span> #title#',
  labels: {
    finish: 'Submit'
  },
  onStepChanging: function (event, currentIndex, newIndex) {
    // Allways allow previous action even if the current form is not valid!
    if (currentIndex > newIndex) {
      return true;
    }
    form.validate().settings.ignore = ":disabled,:hidden";
    return form.valid();
  },
  onFinishing: function (event, currentIndex) {
    form.validate().settings.ignore = ":disabled";
    return form.valid();
  },
  onFinished: function (event, currentIndex) {
    alert("Submitted!");
  }
});

// Initialize validation
stepsValidation.validate({
  ignore: 'input[type=hidden]', // ignore hidden fields
  errorClass: 'danger',
  successClass: 'success',
  highlight: function (element, errorClass) {
    $(element).removeClass(errorClass);
  },
  unhighlight: function (element, errorClass) {
    $(element).removeClass(errorClass);
  },
  errorPlacement: function (error, element) {
    error.insertAfter(element);
  },
  rules: {
    email: {
      required: true,
      email: true
    }
  }
});

// live Icon color change on state change
$(document).ready(function () {
  $(".current").find(".step-icon").addClass("bx bx-time-five");
  $(".current").find(".fonticon-wrap .livicon-evo").updateLiviconEvo({
    strokeColor: '#5A8DEE'
   });

  $('input.tax_included').on('click', function () {
    if ($(this).is(':checked')) {
      // required="required"
      $('#government_tax').required="required"
      $('#resort_fee').required="required"
      $('#service_tax').required="required"
      $('#government_tax').val(0);
      $('#resort_fee').val(0);
      $('#service_tax').val(0);
    }else{
      $('#government_tax').required=""
      $('#resort_fee').required=""
      $('#service_tax').required=""
    }
  });
  
  // Basic Select2 select
  $(".select2").select2({
    // the following code is used to disable x-scrollbar when click in select input and
    // take 100% width in responsive also
    dropdownAutoWidth: true,
    width: '100%'
  });

  $('.select2tag').select2({
    tags: true,
    multiple: true,
    width: '100%',
    // placeholder: "Use , to seperate",
    allowClear: true,
    // minimumInputLength: 2,
    tokenSeparators: [","],
    // data: jQuery.parseJSON(responce.d)
});

  $('.pickatime').pickatime();
 
  $("#cityName").autocomplete({
    source: "/supplier/get_hotelCitys/",
    minLength: 2,
    autoFocus: true,
      select: function( event, ui ) {
      $("input[name='cityid']").val(ui.item.id);  
      $("input[name='hotel_city']").val(ui.item.city_name);
      $("input[name='hotel_country']").val(ui.item.country_name);        
    }
    });
  
  
});

// Icon change on state
// if click on next button icon change
$(".actions [href='#next']").click(function () {
  $(".done").find(".step-icon").removeClass("bx bx-time-five").addClass("bx bx-check-circle");
  $(".current").find(".step-icon").removeClass("bx bx-check-circle").addClass("bx bx-time-five");
  // live icon color change on next button's on click
  $(".current").find(".fonticon-wrap .livicon-evo").updateLiviconEvo({
    strokeColor: '#5A8DEE'
  });
  $(".current").prev("li").find(".fonticon-wrap .livicon-evo").updateLiviconEvo({
    strokeColor: '#39DA8A'
  });
});
$(".actions [href='#previous']").click(function () {
  // live icon color change on next button's on click
  $(".current").find(".fonticon-wrap .livicon-evo").updateLiviconEvo({
    strokeColor: '#5A8DEE'
  });
  $(".current").next("li").find(".fonticon-wrap .livicon-evo").updateLiviconEvo({
    strokeColor: '#adb5bd'
  });
});
// if click on  submit   button icon change
$(".actions [href='#finish']").click(function () {
  $(".done").find(".step-icon").removeClass("bx-time-five").addClass("bx bx-check-circle");
  $(".last.current.done").find(".fonticon-wrap .livicon-evo").updateLiviconEvo({
    strokeColor: '#39DA8A'
  });
});
// add primary btn class
$('.actions a[role="menuitem"]').addClass("btn btn-primary");
$('.icon-tab [role="menuitem"]').addClass("glow ");
$('.wizard-vertical [role="menuitem"]').removeClass("btn-primary").addClass("btn-light-primary");

codeAddress('Pudding Ln, Chigwell IG7 6BY, UK')
function codeAddress(address) {
  var default_pos = {lat: -25.344, lng: 131.036};
  geocoder =  new google.maps.Geocoder();
  $('#hotel_location').val(address);
      geocoder.geocode({'address': address}, function(results, status) {
        if (status === 'OK') {
          $('#google_map').locationpicker({
            location: {latitude: results[0].geometry.location.lat(), longitude: results[0].geometry.location.lng()}, 
            radius: 300,
            inputBinding: {
              latitudeInput: $('#hotel_Latitude'),
              longitudeInput: $('#hotel_Longitude'),
              locationNameInput: $('#hotel_location')
            },
            enableAutocomplete: true,
            onchanged: function (currentLocation, radius, isMarkerDropped) {
                // Uncomment line below to show alert on each Location Changed event
                // alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
            }
          });
          
        } else {
          alert('Geocode was not successful for the following reason: ' + status);
          $('#google_map').locationpicker({
            location: {latitude: default_pos.lat, longitude: default_pos.lng()}, 
            radius: 300,
            inputBinding: {
              latitudeInput: $('#hotel_Latitude'),
              longitudeInput: $('#hotel_Longitude'),
              locationNameInput: $('#hotel_location')
            },
            enableAutocomplete: true,
            onchanged: function (currentLocation, radius, isMarkerDropped) {
                // Uncomment line below to show alert on each Location Changed event
                // alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
            }
          });
        }
      });
    }

    if (window.File && window.FileList && window.FileReader) {
      $(".imageupload").on('change', function () {
             var countFiles = $(this)[0].files.length;
             var imgPath = $(this)[0].value;
             var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
             var image_holder = $(".priview_primaryImg");
             image_holder.empty();
    
          var files = !!this.files ? this.files : [];
          if (!files.length || !window.FileReader) return false; // no file selected, or no FileReader support
    
           if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
               if (typeof (FileReader) != "undefined") {
       
                   for (var i = 0; i < countFiles; i++) {
       
                       var reader = new FileReader();
                       reader.onload = function (e) {
                          var file = e.target;
                          $('<div class="priv_div" style="position:relative;display:inline-block"><img src="'+e.target.result+'" title="'+file.name+'" class="thumbimage" /><i class="bx bxs-phone delete_img"></i></div>').appendTo(image_holder);
                          $(".delete_img").click(function(){
                            countFiles -= 1;
                            $(this).parent(".priv_div").remove();
                            $('.imageupload').val(countFiles);
                          });
                       }
       
                       image_holder.show();
                       reader.readAsDataURL($(this)[0].files[i]);
                   }
       
               } else {
                   alert("It doesn't supports");
               }
           } else {
               alert("Select Only images");
           }
      });
       $(".imageupload1").on('change', function () {
           var countFiles = $(this)[0].files.length;
           var imgPath = $(this)[0].value;
           var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
           var image_holder = $(".preview_gallery");
           image_holder.empty();
    
          var files = !!this.files ? this.files : [];
          if (!files.length || !window.FileReader) return false; // no file selected, or no FileReader support
    
           if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
               if (typeof (FileReader) != "undefined") {
       
                   for (var i = 0; i < countFiles; i++) {
       
                       var reader = new FileReader();
                       reader.onload = function (e) {
                          var file = e.target;
                          $('<div class="priv_div" style="position:relative;display:inline-block"><img src="'+e.target.result+'" title="'+file.name+'" class="thumbimage" /><i class="bx bxs-phone delete_img"></i></div>').appendTo(image_holder);
                          $(".delete_img").click(function(){
                            countFiles -= 1;
                            $(this).parent(".priv_div").remove();
                            $('.imageupload1').val(countFiles);
                          });
                       }
       
                       image_holder.show();
                       reader.readAsDataURL($(this)[0].files[i]);
                   }
       
               } else {
                   alert("It doesn't supports");
               }
           } else {
               alert("Select Only images");
           }
      });
    } else {
      alert("Your browser doesn't support to File API")
    } 