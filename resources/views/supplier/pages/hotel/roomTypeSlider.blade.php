@section('sidebar-content')
<div class="todo-sidebar d-flex">
    <span class="sidebar-close-icon">
      <i class="bx bx-x"></i>
    </span>
    <!-- todo app menu -->
    <div class="todo-app-menu">
      <div class="form-group text-center add-task">
        <!-- new task button -->
        <button type="button" class="btn btn-primary add-task-btn btn-block my-1">
          <i class="bx bx-plus"></i>
          <span>Add Room Type</span>
        </button>
      </div>
      <!-- sidebar list start -->
      <div class="sidebar-menu-list">
       
      </div>
      <!-- sidebar list end -->
    </div>
  </div>
  <!-- todo new task sidebar -->
  <div class="todo-new-task-sidebar">
    <div class="card shadow-none p-0 m-0">
      <div class="card-header border-bottom py-75">
        <div class="task-header d-flex justify-content-between align-items-center">
          <h5 class="new-task-title mb-0">Room Type</h5>
          <h5 class="edit-task-title mb-0">Room Type</h5>
          
        </div>
        <button type="button" class="close close-icon">
          <i class="bx bx-x"></i>
        </button>
      </div>
      <!-- form start -->
      <form id="compose-form" class="mt-1">
        <div class="card-content">
          <div class="card-body py-0 border-bottom">
            <div class="form-group">
              <!-- text area for task title -->
              <input name="title" class="form-control task-title" cols="1" rows="2" placeholder=""
                required>
            </div>
            
          </div>
          <div class="card-body pb-1">
            <div class="mt-1 d-flex justify-content-end">
              <button type="button" class="btn btn-primary add-todo">Add Room Type</button>
              <button type="button" class="btn btn-primary update-todo">Save Changes</button>
            </div>
          </div>
        </div>
      </form>
      <!-- form start end-->
    </div>
  </div>

@endsection
