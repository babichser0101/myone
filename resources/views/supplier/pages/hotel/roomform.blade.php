@extends('supplier.layouts.contentLayoutMaster')
{{-- title --}}
@section('title','Add Room')
{{-- vendor scripts --}}
@section('vendor-styles')
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="stylesheet" type="text/css" href="{{asset('assets/supplier/vendors/css/forms/select/select2.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('assets/supplier/vendors/css/pickers/pickadate/pickadate.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('assets/supplier/vendors/css/pickers/daterange/daterangepicker.css')}}">
@endsection
{{-- page style --}}
@section('page-styles')
<!-- <link rel="stylesheet" type="text/css" href="{{asset('assets/supplier/css/pages/hotel/add_room.css')}}"> -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/supplier/css/pages/hotel/form.css')}}">
@endsection

@section('content')
<!-- Form wizard with icon tabs section start -->
<section id="icon-tabs">
     <form action="{{$room_data ? '/supplier/update_room':'/supplier/add_room'}}" method="post" class="form icon-tab mt-2">
     @csrf
              <fieldset>
                <div class="row">
                  <div class="col-12">
                    <div class="card">
                      <div class="card-content">
                        <div class="card-body row">
                          <div class="col-12">
                            <h6 class="py-50">General Details</h6>
                          </div>
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label>Hotel Name:</label>
                              <select type="text" name="hotel_name"  class="form-control hotel_name" value="{{$room_data? $room_data->hotel_code : ''}}" required>
                                <option>Select Hotel</option>
                                @foreach($hotels as $hotel)
                                  <option value="{{$hotel->hotel_code}}" {{$room_data? $room_data->hotel_code == $hotel->hotel_code ? 'selected':'' : ''}}>{{$hotel->hotel_name}}</option>
                                @endforeach
                              </select>
                            </div>
                          </div>
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label>Room Name:</label>
                              <input name="roomname" id="roomname"  type="text" class="form-control" value="{{$room_data? $room_data->room_name : ''}}" required>
                            </div>
                          </div>
                          <div class="col-sm-4">
                            <div class="form-group">
                              <label>Room Type:</label>
                              <select type="room_type" name="room_type"  class="form-control room_type" value="">
                                <option>Select Room Type</option>
                                @foreach($room_types as $room_type)
                                  <option value="{{$room_type->id}}" {{$room_data? $room_data->hotel_code == $room_type->id ? 'selected':'' : ''}}>{{$room_type->room_type}}</option>
                                @endforeach
                              </select>
                            </div>
                          </div>
                          <div class="col-sm-12">
                            <div class="form-group">
                              <label>Room Description :</label>
                              <textarea name="room_detail" class="form-control" rows="4" id = "room_detail" value="{{$room_data? $room_data->room_desc : ''}}" required></textarea>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-12">
                    <div class="card">
                      <div class="card-content">
                        <div class="card-body row">
                          <div class="col-sm-12">
                            <div class="form-group">
                              <label class="d-block">Meal Plan </label>
                              <div class="row">
                              @foreach($mail_plan as $plan)
                                <div class="col-sm-4">
                                    <input type="checkbox" name="mealplan[]" id="{{$plan->id}}" value="{{$plan->id}}" class="tax_included" checked>
                                    <label for="checkbox{{$plan->id}}">{{$plan->meal_plan}}</label>
                                </div>
                              @endforeach
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-12">
                    <div class="card">
                      <div class="card-content">
                        <div class="card-body row">
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label>Children Min Age Limit :</label>
                              <select name="child_min_age" id="child_min_age" value="" class="form-control">
                                <option value='3'>3</option>
                              </select>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label>Children Max Age Limit :</label>
                              <select name="child_max_age" id="child_max_age" class="form-control">
                                @for ($i = 4; $i < 13; $i++)
                                  <option value="{{ $i }}" {{$room_data? $room_data->childagemaxlimit == $i ? 'selected':'' : ''}}>{{ $i }}</option>
                                @endfor
                              </select>
                            </div>
                          </div>
                          <div class="col-sm-2">
                            <div class="form-group">
                              <label>Min Adults :</label>
                              <select name="adults_min" id="adults_min"  class="form-control">
                                <option value='1'>1</option>
                              </select>
                            </div>
                          </div>
                          <div class="col-sm-2">
                            <div class="form-group">
                              <label>Max Adults :</label>
                              <select name="adults_max" id="adults_max" class="form-control" >
                                <option>Select</option>
                                @for ($i = 1; $i < 11; $i++)
                                  <option value="{{ $i }}" {{$room_data? $room_data->maxadult == $i ? 'selected':'' : ''}}>{{ $i }}</option>
                                @endfor
                              </select>
                            </div>
                          </div>
                          <div class="col-sm-2">
                            <div class="form-group">
                              <label>Min Children :</label>
                              <select name="childerns_min" id="childerns_min" class="form-control">
                                @for ($i = 0; $i < 7; $i++)
                                  <option value="{{ $i }}" {{$room_data? $room_data->minchild == $i ? 'selected':'' : ''}} >{{ $i }}</option>
                                @endfor
                              </select>
                            </div>
                          </div>
                          <div class="col-sm-2">
                            <div class="form-group">
                              <label>Max Children :</label>
                              <select name="childerns_max" id="childerns_max" class="form-control">
                                @for ($i = 0; $i < 7; $i++)
                                  <option value="{{ $i }}" {{$room_data? $room_data->maxchild == $i ? 'selected':'' : ''}}>{{ $i }}</option>
                                @endfor
                              </select>
                            </div>
                          </div>
                          <div class="col-sm-2">
                            <div class="form-group">
                              <label>Min Persons :</label>
                              <select name="persons_min" id="persons_min" class="form-control">
                                @for ($i = 1; $i < 17; $i++)
                                  <option value="{{ $i }}" {{$room_data? $room_data->minperson == $i ? 'selected':'' : ''}}>{{ $i }}</option>
                                @endfor
                              </select>
                            </div>
                          </div>
                          <div class="col-sm-2">
                            <div class="form-group">
                              <label>Max Persons :</label>
                              <select name="persons_max" id="persons_max" class="form-control">
                                @for ($i = 1; $i < 17; $i++)
                                  <option value="{{ $i }}" {{$room_data? $room_data->maxperson == $i ? 'selected':'' : ''}}>{{ $i }}</option>
                                @endfor
                              </select>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-12">
                    <div class="card">
                      <div class="card-content">
                        <div class="card-body row">
                          <div class="col-sm-12">
                            <div class="form-group">
                              <label class="d-block">Meal Plan </label>
                              <div class="row">
                                @foreach($facilities as $facility)
                                  <div class="col-sm-4">
                                      <input type="checkbox" name="facilities[]" id="{{$facility->id}}" value="{{$facility->id}}" class="tax_included" >
                                      <label for="checkbox{{$facility->id}}">{{$facility->facility}}</label>
                                  </div>
                                @endforeach
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </fieldset>
              
              </fieldset> -->
                  <div class="col-sm-12 d-flex justify-content-end">
                    <button type="submit" class="btn btn-primary mr-1 mb-1" id="add_rood_button">{{$room_data?  'Update':'Submit'}} </button>

                    <!-- <button type="update" class="btn btn-primary mr-1 mb-1">Submit</button> -->
                  </div>
            </form>
         
</section>
<!-- Form wizard with number tabs section end -->

@endsection
{{-- vendor scripts --}}
@section('vendor-scripts')
<script src="{{asset('assets/supplier/vendors/js/extensions/jquery.steps.min.js')}}"></script>
<script src="{{asset('assets/supplier/vendors/js/forms/validation/jquery.validate.min.js')}}"></script>
<script src="{{asset('assets/supplier/vendors/js/forms/select/select2.full.min.js')}}"></script>
<script src="{{asset('assets/supplier/vendors/js/extensions/jquery.steps.min.js')}}"></script>
<script src="{{asset('assets/supplier/vendors/js/forms/validation/jquery.validate.min.js')}}"></script>
@endsection

{{-- page scripts --}}
@section('page-scripts')
<script src="{{asset('assets/supplier/js/scripts/pages/hotel/add_room.js')}}"></script>
@endsection
