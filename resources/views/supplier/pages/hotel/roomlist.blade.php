@extends('supplier.layouts.contentLayoutMaster')
{{-- page title --}}
@section('title','room List')
{{-- vendor styles --}}
@section('vendor-styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/supplier/vendors/css/tables/datatable/datatables.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/supplier/vendors/css/forms/select/select2.min.css')}}">
@endsection
{{-- page styles --}}
@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/supplier/css/pages/hotel/roomlist.css')}}">
@endsection
@section('content')
<!-- room list start -->
<section class="room-list-wrapper">
  <div class="room-list-filter px-1">
    <form>
      <div class="row border rounded py-2 mb-2">
        <div class="col-12 col-sm-10">
          <label for="room-list-verified">Hotel Code</label>
          <fieldset class="form-group">
            <select class="form-control search_hotelcode" id="room-list-verified">
              <option>Select Hotel code</option>
              @foreach($hotel_codes as $hotel_code)
                <option value="{{$hotel_code}}">{{$hotel_code}}</option>
              @endforeach
            </select>
          </fieldset>
        </div>
        
        <div class="col-12 col-sm-2 d-flex align-items-center">
          <button type="reset" class="btn btn-primary btn-block glow room-list-clear mb-0">Clear</button>
        </div>
      </div>
    </form>
  </div>
  <div class="room-list-table">
    <div class="card">
      <div class="card-content">
        <div class="card-body">
          <!-- datatable start -->
          <div class="table-responsive">
            <table id="room-list-datatable" class="table">
              <thead>
                <tr>
                    <th>id</th>
                    <th>Hotel Code</th>
                    <th>Hotel Name</th>
                    <th>Room Name</th>
                    <th>Status</th>
                    <th>Action</th>
                    <th>Add/Edit Room Gallery Image</th>
                    <th>Edit</th>
                    <th>Manage Rate Status</th>
                </tr>
              </thead>
              <tbody>
              @foreach($roomlists as $key=>$room)
                <tr>
                  <td>{{$room->supplier_room_list_id}}</td>
                  <td>{{$room->hotel_code}}</td>
                  <td>{{$hotels_names[$key]->hotel_name}}</td>
                  <td>{{$room->room_name}}</td>
                  <td><span class="badge {{ $room->status ? ' badge-light-danger' : 'badge-light-success' }} status_alert{{$room->supplier_room_list_id}}" data-status="{{$room->status}}">{{ $room->status ? 'Inactive' : 'Active' }}</span></td>
                  <th>
                    <a href="#!" class="btn btn-sm {{ $room->status ? 'btn-success' : 'btn-danger' }} glow room_activate" id="state_btn_{{$room->supplier_room_list_id}}" data-id = "{{$room->supplier_room_list_id}}">
                      <i class="bx {{ $room->status ? ' bx-time' : ' bx-bug' }}"></i><span>&nbsp;{{ $room->status ? 'Active' : 'InActive' }}</span>
                    </a>
                  </th>
                  <th>
                    <a href="#!" class="btn btn-sm btn-warning glow">
                      <i class="bx bx-box"></i>&nbsp;Add/Edit Room Gallery Image
                    </a>
                  </th>
                  <th>
                    <a href="/supplier/roomEdit/{{$room->supplier_room_list_id}}" class="btn btn-sm btn-primary glow">
                      <i class="bx bx-edit-alt"></i>&nbsp;Edit
                    </a>
                  </th>
                  <th>
                    <a href="#!" class="btn btn-sm btn-black glow">
                      <i class="bx bx-edit-alt"></i>&nbsp;Manage Rate_Status
                    </a>
                  </th>
                </tr>
                @endforeach
              </tbody>
            </table>
            
          </div>
          <!-- datatable ends -->
        </div>
      </div>
    </div>
  </div>
</section>
<!-- room list ends -->
@endsection

{{-- vendor scripts --}}
@section('vendor-scripts')
<script src="{{asset('assets/supplier/vendors/js/tables/datatable/datatables.min.js')}}"></script>
<script src="{{asset('assets/supplier/vendors/js/tables/datatable/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/supplier/vendors/js/forms/select/select2.full.min.js')}}"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
@endsection

{{-- page scripts --}}
@section('page-scripts')
<script src="{{asset('assets/supplier/js/scripts/pages/hotel/roomlist.js')}}"></script>
@endsection
