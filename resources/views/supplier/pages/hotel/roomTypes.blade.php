@extends('supplier.layouts.contentLayoutMaster')

{{-- page title --}}
@section('title','ROOM TYPE')
{{-- vendor styles --}}
@section('vendor-styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/supplier/vendors/css/pickers/daterange/daterangepicker.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/supplier/vendors/css/forms/select/select2.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/supplier/vendors/css/editors/quill/quill.snow.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/supplier/vendors/css/extensions/dragula.min.css')}}">
@endsection
{{-- page style --}}
@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/supplier/css/pages/hotel/roomType.css')}}">
@endsection
{{-- sidebar --}}
@include('supplier.pages.hotel.roomTypeSlider')
{{-- page content --}}
@section('content')
<div class="app-content-overlay"></div>
<div class="todo-app-area">
  <div class="todo-app-list-wrapper">
    <div class="todo-app-list">
      <div class="todo-fixed-search d-flex justify-content-between align-items-center">
        <div class="sidebar-toggle d-block d-lg-none">
          <i class="bx bx-menu"></i>
        </div>
        <fieldset class="form-group position-relative has-icon-left m-0 flex-grow-1">
          <input type="text" class="form-control todo-search" id="todo-search" placeholder="Search Room Type">
          <div class="form-control-position">
            <i class="bx bx-search"></i>
          </div>
        </fieldset>
        <div class="todo-sort dropdown mr-1">
          <button class="btn dropdown-toggle sorting" type="button" id="sortDropdown" data-toggle="dropdown"
            aria-haspopup="true" aria-expanded="false">
            <i class="bx bx-filter"></i>
            <span>Sort</span>
          </button>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="sortDropdown">
            <a class="dropdown-item ascending" href="#">Ascending</a>
            <a class="dropdown-item descending" href="#">Descending</a>
          </div>
        </div>
      </div>
      <div class="todo-task-list list-group">
        <!-- task list start -->
        <ul class="todo-task-list-wrapper list-unstyled" id="todo-task-list-drag">
        @foreach ($room_type_list as $room_item)
          <li class="todo-item" id="roomType_{{$room_item->id}}" data-status="{{$room_item->status}}" data-id="{{$room_item->id}}">
            <div class="todo-title-wrapper d-flex justify-content-sm-between justify-content-end align-items-center">
              <div class="todo-title-area d-flex">
                <i class='bx bx-grid-vertical handle'></i>
                <p class="todo-title mx-50 m-0 truncate">{{$room_item->room_type}}</p>
              </div>
              <div class="todo-item-action d-flex align-items-center" data-name="{{$room_item->id}}">
                <div class="todo-badge-wrapper d-flex">
                @if ($room_item->status === '2')
                  <span class="badge {{$room_item->status=='2'? 'badge-light-secondary' :  'badge-light-danger'}} badge-pill" id='status_span{{$room_item->id}}'>{{$room_item->status==2? 'blocked' :'inactive'}}</span>
                @else
                  <span class="badge {{$room_item->status==1? 'badge-light-primary' : 'badge-light-danger'}} badge-pill" id='status_span{{$room_item->id}}'>{{$room_item->status==2? 'blocked' : $room_item->status==1? 'active':'inactive'}}</span>
                  <!-- <span class="badge {{$room_item->status=='2'? 'badge-light-secondary' :  'badge-light-danger'}} badge-pill" id='status_span{{$room_item->id}}'>{{$room_item->status==2? 'blocked' :'inactive'}}</span> -->
                @endif
                </div>
                <a class='todo-item-favorite ml-75 {{$room_item->status==1? "danger":"warning"}}' data-id="{{$room_item->id}}" id="activeBtn_{{$room_item->id}}" ><i class="bx bx-star"></i> <span>{{$room_item->status==1? 'inactive':'active'}}</span></a>
                <a class='todo-item-block ml-75 {{$room_item->status==2? "danger":""}}' data-id="{{$room_item->id}}" id="blockBtn_{{$room_item->id}}" ><i class="bx bx-trash"></i> <span>{{$room_item->status==2? "blocked":"block"}} </span></a>
              </div>
            </div>
          </li>
          @endforeach
        </ul>
        <!-- task list end -->
        <div class="no-results">
          <h5>No Items Found</h5>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('vendor-scripts')
    <script src="{{asset('assets/supplier/vendors/js/pickers/daterange/moment.min.js')}}"></script>
    <script src="{{asset('assets/supplier/vendors/js/pickers/daterange/daterangepicker.js')}}"></script>
    <script src="{{asset('assets/supplier/vendors/js/forms/select/select2.full.min.js')}}"></script>
    <script src="{{asset('assets/supplier/vendors/js/editors/quill/quill.min.js')}}"></script>
    <script src="{{asset('assets/supplier/vendors/js/extensions/dragula.min.js')}}"></script>
@endsection

{{-- page scripts --}}
@section('page-scripts')
<script src="{{asset('assets/supplier/js/scripts/pages/hotel/roomType.js')}}"></script>
@endsection
