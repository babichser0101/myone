@extends('supplier.layouts.contentLayoutMaster')

{{-- title --}}
 
@section('title', 'Add Hotel')
  
{{-- vendor scripts --}}
@section('vendor-styles')
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="stylesheet" type="text/css" href="{{asset('assets/supplier/vendors/css/forms/select/select2.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('assets/supplier/vendors/css/pickers/pickadate/pickadate.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('assets/supplier/vendors/css/pickers/daterange/daterangepicker.css')}}">
@endsection
  
{{-- page style --}}
@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/supplier/css/pages/hotel/form.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/supplier/css/jquery-ui.css')}}">
@endsection

@section('content')
<!-- Form wizard with icon tabs section start -->
<section id="info-tabs-">
  <form action="#" class="wizard-horizontal icon-tab mt-2">
    <!-- Step 1 -->
    <h6>
      <i class="step-icon"></i>
      <span class="fonticon-wrap">Step 1</span>
      <span class="pl-1">General</span>
    </h6>
    <!-- Step 1 end-->
    <!-- body content of step 1 -->
    <fieldset>
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-content">
              <div class="card-body row">
                <div class="col-12">
                  <h6 class="py-50">General Details</h6>
                </div>
                <div class="col-sm-4">
                  <div class="form-group">
                    <label>Property Name:</label>
                    <input type="text" name="hotel_property_name"  class="form-control" value="{{$editHotel? $editHotel->hotel_name : ''}}" >
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="form-group">
                    <label>Property Type:</label>
                    <select name="hotel_property_type"  id="hotel_property_type" class="form-control select2" required>
                      <option value="1" selected>Hotel</option>
                    </select>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="form-group">
                    <label>Property Rating:</label>
                    <select type="number" name="hotel_star_rating"  class="form-control" value="{{$editHotel? $editHotel->hotel_star_rating : ''}}">
                      <option value="0.5">0.5</option>
                      <option value="0.5">1</option>
                      <option value="1.5">1.5</option>
                      <option value="2.5">2</option>
                      <option value="2.5">2.5</option>
                      <option value="3.5">3</option>
                      <option value="3.5">3.5</option>
                      <option value="4.5">4</option>
                      <option value="4.5">4.5</option>
                      <option value="4.5">5</option>
                    </select>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="form-group">
                    <label>Currency:</label>
                    <select name="currency_type" class="form-control select2" required>
                      <option value="USD" selected>USD</option>
                      
                    </select>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="form-group">
                    <label>Destination: e.g ( Montego Bay, Jamaica):</label>
                    <input name="cityName" id="cityName"  type="text" class="form-control" value="{{ $editHotel? $editHotel->hotel_city.' '.$editHotel->hotel_country : ''}}" required>
                    <input name="hotel_city" id="hotel_city"  type="hidden" class="form-control" value="{{ $editHotel? $editHotel->hotel_city: ''}}" required>
                    <input name="hotel_country" id="hotel_country"  type="hidden" class="form-control" value="{{ $editHotel? $editHotel->hotel_country : ''}}" required>
                    <input name="cityid" id="cityid"  type="hidden" class="form-control" value="{{ $editHotel? $editHotel->cityid : ''}}" required>
                  </div>
                </div>
                <div class="col-sm-12">
                  <div class="form-group">
                    <label>Address:</label>
                    <textarea name="address" class="form-control" rows="4" id = "hotel_prev_address" value="{{ $editHotel? $editHotel->address : ''}}" required></textarea>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-content">
              <div class="card-body row">
                <div class="col-12">
                  <h6 class="py-50">Taxes Details</h6>
                </div>
                <div class="col-sm-12">
                  <div class="form-group">
                    <div class="c-inputs-stacked">
                      <div class="d-inline-block mr-2">
                        <fieldset>
                          <div class="checkbox">
                            <input type="checkbox" class="tax_included" id="checkbox6" value="{{ $editHotel? $editHotel->tax_included : '0'}}" >
                            <input type="hidden" class="tax_include_val" value='no' name="tax_include" >
                            <label for="checkbox6">Tax included</label>
                          </div>
                        </fieldset>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="form-group">
                    <label>Government Tax(%):</label>
                    <input name="government_tax" id="government_tax" value="" type="number" data-parsley-type="number" class="form-control"  value="{{ $editHotel? $editHotel->government_tax : ''}}"  min="0" max="50">
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="form-group">
                    <label>Resort Fee(fixed):</label>
                    <input name="resort_fee" id="resort_fee" value="" type="number" data-parsley-type="number" class="form-control" value="{{ $editHotel? $editHotel->resort_fee : ''}}" min="0" max="50">
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="form-group">
                    <label>Service Tax(%):</label>
                    <input name="service_tax" id="service_tax" value="" type="number" class="form-control" data-parsley-type="number" value="{{ $editHotel? $editHotel->service_tax : ''}}" min="0" max="50">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </fieldset>
    <!-- body content of step 1 end-->

    <!-- Step 2-->
    <h6>
      <i class="step-icon"></i>
      <span class="fonticon-wrap">Step 2</span>
      <span class="pl-1">Contact Info</span>
    </h6>
    <!-- Step 2 end-->
    <!-- body content of step 2 -->
    <fieldset>
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-content">
              <div class="card-body row">
                <div class="col-12">
                  <h6 class="py-50">Location Details</h6>
                </div>
                <div class="col-sm-4">
                  <div class="form-group">
                    <label>Location:</label>
                    <input type="text" name="hotel_location" id="hotel_location" value="{{ $editHotel? $editHotel->location : ''}}" class="form-control">
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="form-group">
                    <label>Latitude:</label>
                    <input type="text" name="hotel_Latitude" id="hotel_Latitude" value="{{ $editHotel? $editHotel->latitude : ''}}" class="form-control">
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="form-group">
                    <label>Longitude:</label>
                    <input type="text" name="hotel_Longitude" id="hotel_Longitude" value="{{ $editHotel? $editHotel->longitude : ''}}" class="form-control">
                  </div>
                </div>
                <div class="col-12" > 
                  <div class="google_map" id="google_map" style = "width: 92vw; height: 500px;">
                  
                  </div>
                </div>
              </div>
              
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-content">
              <div class="card-body row">
                <div class="col-12">
                  <h6 class="py-50">Hotel Details</h6>
                </div>
                <div class="col-sm-4">
                  <div class="form-group">
                    <label>Hotel Email:</label>
                    <input type="text" name="hotel_email" id="hotel_email" class="form-control" value="{{ $editHotel? $editHotel->hotel_email : ''}}" required>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="form-group">
                    <label>Reservation email:</label>
                    <input type="text" name="hotel_re_email" id="hotel_re_email" class="form-control" value="{{ $editHotel? $editHotel->reservation_email : ''}}" required>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="form-group">
                    <label>Hotel Phone:</label>
                    <input type="text" name="hotel_phone" id="hotel_phone" class="form-control" value="{{ $editHotel? $editHotel->hotel_phone : ''}}" required>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="form-group">
                    <label>Hotel Fax:</label>
                    <input type="text" name="hotel_fax" id="hotel_fax" class="form-control" value="{{ $editHotel? $editHotel->hotel_fax : ''}}" required>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="form-group">
                    <label>Booking Phone:</label>
                    <input type="text" name="booking_phone" id="booking_phone" class="form-control" value="{{ $editHotel? $editHotel->booking_phone : ''}}" required>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="form-group">
                    <label>Emergency Number:</label>
                    <input type="text" name="emergency_number" id="emergency_number" class="form-control" value="{{ $editHotel? $editHotel->emergency_no : ''}}" required>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="form-group">
                    <label>TripAdvisor location Id:</label>
                    <input type="number" name="ta_locationId" id="ta_locationId" class="form-control" value="{{ $editHotel? $editHotel->ta_location_id : ''}}" required>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </fieldset>
    <!-- body content of step 2 end-->

    <!-- Step 3-->
    <h6>
      <i class="step-icon"></i>
      <span class="fonticon-wrap">Step 3</span>
      <span class="pl-1">Hotel Amenities</span>
    </h6>
    <!-- Step 3 end-->
    <!-- body content of step 3 -->
    <fieldset>
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-content">
              <div class="card-body row">
                <div class="col-12">
                  <h6 class="py-50">Hotel Amenities</h6>
                </div>
                @foreach($amentity_types as $amentity)
                  <div class="col-sm-4">
                    <div class="form-group">
                      <div class="c-inputs-stacked">
                        <div class="d-inline-block mr-2">
                          <fieldset>
                            <div class="checkbox">
                              <input type="checkbox" name="hotel_facilities[]" class="tax_included" value="{{$amentity->id}}" id="checkbox{{$amentity->id}}"  {{$editHotel? strpos($editHotel->hotel_facilities, strval($amentity->id))==false ? '':'checked' : $amentity->is_edit ? '':'checked'}} >
                              <label for="checkbox{{$amentity->id}}">{{$amentity->facility}}</label>
                            </div>
                          </fieldset>
                        </div>
                      </div>
                    </div>
                  </div>
                @endforeach
              </div>
            </div>
          </div>
        </div>
      </div>
    </fieldset>
    <!-- body content of step 3 end-->

    <!-- Step 4-->
    <h6>
      <i class="step-icon"></i>
      <span class="fonticon-wrap">Step 4</span>
      <span class="pl-1">Images</span>
    </h6>
    <!-- Step 4 end-->
    <!-- body content of step 4 -->
    <fieldset>
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-content">
              <div class="card-body row">
                <div class="col-12">
                  <h6 class="py-50">Primary Hotel Image</h6>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                    <span class="btn btn-success fileinput-button">
                      <i class="glyphicon glyphicon-plus"></i>
                      <span>Add image...</span>
                      <input type="file" accept="image/*" class="imageupload" name="uploadfile1" size="20" /><br/>
                    </span>
                    
                  </div>
                </div>
                <div class="col-12">
                  <div class="priview_primaryImg"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-content">
              <div class="card-body row">
                <div class="col-12">
                  <h6 class="py-50">Gallery Images</h6>
                </div>
                <div class="col-12">
                  <span class="btn btn-success fileinput-button">
                    <i class="glyphicon glyphicon-plus"></i>
                    <span>Add Multiple image...</span>
                    <input type="file" multiple="multiple" accept="image/*" class="imageupload1" name="uploadfile2[]" /><br/>
                  </span>
                </div>
                <div class="col-12 preview_gallery">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </fieldset>
    <!-- body content of step 4 end-->

    <!-- Step 5-->
    <h6>
      <i class="step-icon"></i>
      <span class="fonticon-wrap">Step 5</span>
      <span class="pl-1">Property Details</span>
    </h6>
    <!-- Step 5 end-->
    <!-- body content of step 5 -->
    <fieldset>
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-content">
              <div class="card-body row">
                <div class="col-12">
                  <h6 class="py-50">Check In/Out</h6>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label>Check-in:</label>
                    <fieldset class="form-group position-relative has-icon-left">
                      <input type="text" name="check_in" value="{{ $editHotel? $editHotel->check_in : ''}}" class="form-control pickatime" />
                      <div class="form-control-position">
                          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" style="fill: #727E8C" viewBox="0 0 24 24"><path d="M12 8L12 13 17 13 17 11 14 11 14 8z"/><path d="M21.292,8.497c-0.226-0.535-0.505-1.05-0.829-1.529c-0.322-0.478-0.691-0.926-1.099-1.333S18.51,4.86,18.032,4.537 c-0.482-0.326-0.997-0.604-1.528-0.829c-0.545-0.23-1.114-0.407-1.69-0.525c-1.181-0.243-2.444-0.244-3.626,0 c-0.579,0.118-1.147,0.295-1.69,0.524c-0.531,0.225-1.045,0.503-1.529,0.83C7.492,4.859,7.043,5.229,6.636,5.636 C6.229,6.043,5.859,6.492,5.537,6.968C5.211,7.452,4.932,7.966,4.708,8.496c-0.23,0.544-0.407,1.113-0.525,1.69 C4.062,10.778,4,11.388,4,12c0,0.008,0.001,0.017,0.001,0.025H2L5,16l3-3.975H6.001C6.001,12.017,6,12.008,6,12 c0-0.477,0.048-0.952,0.142-1.412c0.092-0.449,0.229-0.89,0.408-1.313c0.174-0.412,0.391-0.813,0.645-1.188 C7.445,7.716,7.733,7.368,8.05,7.05s0.666-0.605,1.036-0.855c0.376-0.254,0.777-0.471,1.19-0.646 c0.421-0.179,0.863-0.316,1.313-0.408c0.919-0.189,1.904-0.188,2.823,0c0.447,0.092,0.89,0.229,1.313,0.408 c0.413,0.174,0.813,0.392,1.188,0.644c0.37,0.251,0.72,0.539,1.037,0.856c0.317,0.316,0.604,0.665,0.855,1.037 c0.252,0.372,0.469,0.772,0.645,1.189c0.178,0.417,0.314,0.858,0.408,1.311C19.952,11.049,20,11.524,20,12 s-0.048,0.951-0.142,1.41c-0.094,0.455-0.23,0.896-0.408,1.314c-0.176,0.416-0.393,0.815-0.646,1.189 c-0.25,0.371-0.537,0.72-0.854,1.036c-0.317,0.317-0.667,0.605-1.036,0.855c-0.376,0.253-0.775,0.471-1.189,0.646 c-0.423,0.179-0.865,0.316-1.313,0.408c-0.918,0.188-1.902,0.189-2.823,0c-0.449-0.092-0.89-0.229-1.313-0.408 c-0.412-0.174-0.813-0.391-1.188-0.645c-0.371-0.25-0.719-0.538-1.037-0.855l-1.414,1.414c0.407,0.408,0.855,0.777,1.332,1.099 c0.483,0.326,0.998,0.605,1.528,0.829c0.544,0.23,1.113,0.407,1.69,0.525C11.778,20.938,12.388,21,13,21 c0.612,0,1.223-0.062,1.813-0.183c0.577-0.118,1.146-0.294,1.69-0.524c0.532-0.225,1.047-0.504,1.531-0.831 c0.476-0.322,0.923-0.691,1.33-1.098s0.776-0.855,1.098-1.331c0.325-0.48,0.604-0.995,0.83-1.529 c0.228-0.538,0.405-1.106,0.525-1.692C21.938,13.22,22,12.61,22,12s-0.062-1.22-0.183-1.814 C21.697,9.602,21.52,9.034,21.292,8.497z"/></svg>
                      </div>
                    </fieldset>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label>Check-out:</label>
                    <fieldset class="form-group position-relative has-icon-left">
                      <input type="text" name="check_out" value="{{ $editHotel? $editHotel->check_out : ''}}" class="form-control pickatime" />
                      <div class="form-control-position">
                          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" style="fill: #727E8C" viewBox="0 0 24 24"><path d="M12 8L12 13 17 13 17 11 14 11 14 8z"/><path d="M21.292,8.497c-0.226-0.535-0.505-1.05-0.829-1.529c-0.322-0.478-0.691-0.926-1.099-1.333S18.51,4.86,18.032,4.537 c-0.482-0.326-0.997-0.604-1.528-0.829c-0.545-0.23-1.114-0.407-1.69-0.525c-1.181-0.243-2.444-0.244-3.626,0 c-0.579,0.118-1.147,0.295-1.69,0.524c-0.531,0.225-1.045,0.503-1.529,0.83C7.492,4.859,7.043,5.229,6.636,5.636 C6.229,6.043,5.859,6.492,5.537,6.968C5.211,7.452,4.932,7.966,4.708,8.496c-0.23,0.544-0.407,1.113-0.525,1.69 C4.062,10.778,4,11.388,4,12c0,0.008,0.001,0.017,0.001,0.025H2L5,16l3-3.975H6.001C6.001,12.017,6,12.008,6,12 c0-0.477,0.048-0.952,0.142-1.412c0.092-0.449,0.229-0.89,0.408-1.313c0.174-0.412,0.391-0.813,0.645-1.188 C7.445,7.716,7.733,7.368,8.05,7.05s0.666-0.605,1.036-0.855c0.376-0.254,0.777-0.471,1.19-0.646 c0.421-0.179,0.863-0.316,1.313-0.408c0.919-0.189,1.904-0.188,2.823,0c0.447,0.092,0.89,0.229,1.313,0.408 c0.413,0.174,0.813,0.392,1.188,0.644c0.37,0.251,0.72,0.539,1.037,0.856c0.317,0.316,0.604,0.665,0.855,1.037 c0.252,0.372,0.469,0.772,0.645,1.189c0.178,0.417,0.314,0.858,0.408,1.311C19.952,11.049,20,11.524,20,12 s-0.048,0.951-0.142,1.41c-0.094,0.455-0.23,0.896-0.408,1.314c-0.176,0.416-0.393,0.815-0.646,1.189 c-0.25,0.371-0.537,0.72-0.854,1.036c-0.317,0.317-0.667,0.605-1.036,0.855c-0.376,0.253-0.775,0.471-1.189,0.646 c-0.423,0.179-0.865,0.316-1.313,0.408c-0.918,0.188-1.902,0.189-2.823,0c-0.449-0.092-0.89-0.229-1.313-0.408 c-0.412-0.174-0.813-0.391-1.188-0.645c-0.371-0.25-0.719-0.538-1.037-0.855l-1.414,1.414c0.407,0.408,0.855,0.777,1.332,1.099 c0.483,0.326,0.998,0.605,1.528,0.829c0.544,0.23,1.113,0.407,1.69,0.525C11.778,20.938,12.388,21,13,21 c0.612,0,1.223-0.062,1.813-0.183c0.577-0.118,1.146-0.294,1.69-0.524c0.532-0.225,1.047-0.504,1.531-0.831 c0.476-0.322,0.923-0.691,1.33-1.098s0.776-0.855,1.098-1.331c0.325-0.48,0.604-0.995,0.83-1.529 c0.228-0.538,0.405-1.106,0.525-1.692C21.938,13.22,22,12.61,22,12s-0.062-1.22-0.183-1.814 C21.697,9.602,21.52,9.034,21.292,8.497z"/></svg>
                      </div>
                    </fieldset>
                  </div>
                </div>
                <div class="col-sm-6 offset-sm-6">
                  <div class="form-group">
                    <label>Minimum check-in age:</label>
                    <input type="text" name="min_check_in" value="{{ $editHotel? $editHotel->minimum_check_in : ''}}" class="form-control" required>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label>Hotel description:</label>
                    <textarea rows="4" name="hotel_description" value="{{ $editHotel? $editHotel->hotel_desc : ''}}" class="form-control" ></textarea>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label>Check-in instructions:</label>
                    <textarea rows="4" name="check-in_instro" value="{{ $editHotel? $editHotel->Check_in_instructions : ''}}" class="form-control"></textarea>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-content">
              <div class="card-body row">
                <div class="col-12">
                  <h6 class="py-50">Meal Plan</h6>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label>Meal Plan Description:</label>
                    <textarea rows="4" name="meal_plan_desc" value="{{ $editHotel? $editHotel->meal_plan_desc : ''}}" class="form-control"></textarea>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label>Important information:</label>
                    <textarea rows="4" name="important_information" value="{{ $editHotel? $editHotel->imp_information : ''}}" class="form-control"></textarea>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-content">
              <div class="card-body row">
                <div class="col-12">
                  <h6 class="py-50">Others</h6>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label>Language Spoken:</label>
                    <select class="select2tag form-control" multiple="multiple" name="language[]" value="{{ $editHotel? $editHotel->language : ''}}" required>
                    </select>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label>Restaurants available or close by:</label>
                    <select class="select2tag form-control" multiple="multiple" name="able_restaurant[]" value="{{ $editHotel? $editHotel->close_by : ''}}"></select>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label>Parking Type:</label>
                    <select class="form-control" name="paking_Type" value="{{ $editHotel? $editHotel->hotel_parking_type : ''}}">
                      <option value="">Select Type</option>
                      <option value="free_self_parking">Free Self Parking</option>
                      <option value="paid_parking">Paid Parking</option>
                      <option value="free_limited_parking">Free Limited Parking</option>
                    </select>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label>Are pets allowed:</label>
                    <select class="form-control" name="allow_pets" value="{{ $editHotel? $editHotel->pets_allow : ''}}">
                      <option value="">Are pets allowed:</option>
                      <option value="pets_allowed">Pets allowed</option>
                      <option value="pets_not_allowed">Pets not allowed</option>
                    </select>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label>Nearby things to do:</label>
                    <select class="select2tag form-control" multiple="multiple" name="nearby_things[]" value="{{ $editHotel? $editHotel->nearby : ''}}">
                    </select>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label>Entertainment and family facilities:</label>
                    <select class="select2tag form-control" multiple="multiple" name="entertainment_facilities[]" value="{{ $editHotel? $editHotel->entertainment : ''}}">
                    </select>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label>Pool and Spa:</label>
                    <select class="select2tag form-control" multiple="multiple" name="pool_spa[]" value="{{ $editHotel? $editHotel->pool : ''}}">
                    </select>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label>Transfers available:</label>
                    <select class="select2tag form-control" name="able_transfers[]" value="{{ $editHotel? $editHotel->transfers : ''}}" >
                    </select>
                  </div>
                </div>
                <div class="col-sm-12">
                  <div class="form-group">
                    <label>Internet:</label>
                    <textarea rows="4" class="form-control" name="internet" value="{{ $editHotel? $editHotel->internet : ''}}"></textarea>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </fieldset>
    <!-- body content of step 5 end-->

    <!-- Step 6-->
    <h6>
      <i class="step-icon"></i>
      <span class="fonticon-wrap">Step 6</span>
      <span class="pl-1">Policies</span>
    </h6>
    <!-- Step 6 end-->
    <!-- body content of step 6 -->
    <fieldset>
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-content">
              <div class="card-body row">
                <div class="col-12">
                  <h6 class="py-50">Policy Details</h6>
                </div>
                <div class="col-sm-12">
                  <div class="form-group">
                    <label>Hotel Policy:</label>
                    <textarea rows="4" class="form-control" name="hotel_policy" value="{{ $editHotel? $editHotel->policy : ''}}"></textarea>
                  </div>
                </div>
                <div class="col-sm-12">
                  <div class="form-group">
                    <label>Child Policy:</label>
                    <textarea rows="4" class="form-control" name="child_policy" value="{{ $editHotel? $editHotel->child_policy : ''}}"></textarea>
                  </div>
                </div>
                <div class="col-sm-12">
                  <div class="form-group">
                    <label>Terms and Condition:</label>
                    <textarea rows="4" class="form-control" name="term_condition" value="{{ $editHotel? $editHotel->terms_and_condition : ''}}"></textarea>
                  </div>
                </div>
                <div class="col-sm-12">
                  <div class="form-group">
                    <label>Rate Description:</label>
                    <textarea rows="4" class="form-control" name="rate_description" value="{{ $editHotel? $editHotel->rate_desc : ''}}"></textarea>
                  </div>
                </div>
                <div class="col-sm-12">
                  <div class="form-group">
                    <label>Room Charges Disclosure:</label>
                    <textarea rows="4" class="form-control" name="room_charges" value="{{ $editHotel? $editHotel->room_charge_disclosure : ''}}"></textarea>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </fieldset>
    <!-- body content of step 6 end-->
  </form>

</section>
<!-- Form wizard with icon tabs section end -->

@endsection

{{-- vendor scripts --}}
@section('vendor-scripts')
<script src="{{asset('assets/supplier/vendors/js/extensions/jquery.steps.min.js')}}"></script>
<script src="{{asset('assets/supplier/vendors/js/forms/validation/jquery.validate.min.js')}}"></script>
<script src="{{asset('assets/supplier/vendors/js/forms/select/select2.full.min.js')}}"></script>
<script src="{{asset('assets/supplier/vendors/js/pickers/pickadate/picker.js')}}"></script>
<script src="{{asset('assets/supplier/vendors/js/pickers/pickadate/picker.date.js')}}"></script>
<script src="{{asset('assets/supplier/vendors/js/pickers/pickadate/picker.time.js')}}"></script>
<script src="{{asset('assets/supplier/vendors/js/pickers/pickadate/legacy.js')}}"></script>
<script src="{{asset('assets/supplier/vendors/js/pickers/daterange/moment.min.js')}}"></script>
<script src="{{asset('assets/supplier/vendors/js/pickers/daterange/daterangepicker.js')}}"></script>
@endsection

{{-- page scripts --}}
@section('page-scripts')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyADPfKkRh8XYqiV5gPIzPW1CdXAbR7l9gE&libraries=places"></script>
<script src="{{asset('assets/supplier/js/jquery-ui.js')}}"></script>
<script src="{{asset('assets/supplier/js/locationpicker.jquery.min.js')}}"></script>
<script src="{{asset('assets/supplier/js/scripts/pages/hotel/form.js')}}"></script>

@endsection
