<?php

namespace App\Models\Supplier;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class Supplier extends Authenticatable
{
    use Notifiable,HasRoles;

    protected $table = 'supplier_info';

    protected $guard = 'supplier';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
    ];

    public function hotel_list(){
        return $this->hasMany('App\Models\SupplierHotelList', 'supplier_hotel_list_id', 'id');
    }

    public function hotel_Room_list(){
        return $this->hasMany('App\Models\SupplierRoomList');
    }
    public function vila_list(){
        return $this->hasMany('App\Models\VillaList');
    }
    public function hotel_RoomRate_list(){
        return $this->hasMany('App\Models\SupHotelRoomRatesList');
    }
    public function hotel_facility_types(){
        return $this->hasMany('App\Models\GlbHotelFacilitiesType');
    }
    public function hotel_room_types(){
        return $this->hasMany('App\Models\GlbHotelRoomType');
    }
    public function hotel_mealPan(){

        return $this->hasMany('App\Models\GlbHotelMealPlan');
    } 
}
