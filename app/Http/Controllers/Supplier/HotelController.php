<?php

namespace App\Http\Controllers\Supplier;

use App\Http\Controllers\Controller;
use App\Models\Supplier\HotelList;
use App\Models\GlbHotelPropertyType;
use App\Models\Currency;
use App\Models\GlbHotelFacilitiesType;
use App\Models\JamaicanCityList;
use App\Models\SupplierHotelList;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class HotelController extends Controller
{
    // Hotel List
    public function listHotel()
    {
        $supplier_user = Auth::guard('supplier')->user();
        $pageConfigs = ['pageHeader' => true];

        $breadcrumbs = [
            ["link" => "/", "name" => "Home"],["name" => "Hotels"],["name" => "Hotel List"]
        ];

        $data = [
            'pageConfigs'=>$pageConfigs,
            'breadcrumbs'=>$breadcrumbs,
            'hotelList' => $supplier_user->hotel_list,
            'hotelRatings' => SupplierHotelList::orderBy('hotel_star_rating','asc')->distinct('hotel_star_rating')->pluck('hotel_star_rating'),
            'hotelStatuses' => SupplierHotelList::orderBy('status','desc')->distinct('status')->pluck('status')
        ];

        return view('supplier.pages.hotel.list', $data);
    }

    // Hotel Add
    public function addHotel()
    {
        $supplier_user = Auth::guard('supplier')->user();
        $pageConfigs = ['pageHeader' => true];

        $breadcrumbs = [
            ["link" => "/", "name" => "Home"],["name" => "Hotels"],["name" => "Add Hotel"]
        ];
        $getTypes_datas = GlbHotelFacilitiesType::all();
        $amentity_data = $getTypes_datas->where('facility_type', 'hotel');
        $amentity_data = $amentity_data->where('status', 1);
        $data = [
            'pageConfigs'=>$pageConfigs,
            'breadcrumbs'=>$breadcrumbs,
            'editHotel'=> null,
            'amentity_types'=>$amentity_data,
            'hotel_citys'=>JamaicanCityList::all(),
            'hotelRatings' => SupplierHotelList::orderBy('hotel_star_rating','asc')->distinct('hotel_star_rating')->pluck('hotel_star_rating'),
            'currenies' => Currency::all(),
        ];

        return view('supplier.pages.hotel.form', $data);
    }

    public function editHotel($id){
        $edit_hotel = SupplierHotelList::find($id);
        $pageConfigs = ['pageHeader' => true];

        $breadcrumbs = [
            ["link" => "/", "name" => "Home"],["name" => "Hotels"],["name" => "Edit Hotel"]
        ];
        $getTypes_datas = GlbHotelFacilitiesType::all();
        $amentity_data = $getTypes_datas->where('facility_type', 'hotel');
        $amentity_data = $amentity_data->where('status', 1);
        $data = [
            'pageConfigs'=>$pageConfigs,
            'breadcrumbs'=>$breadcrumbs,
            'editHotel'=>$edit_hotel,
            'amentity_types'=>$amentity_data,
            'hotel_citys'=>JamaicanCityList::all(),
            'hotelRatings' => SupplierHotelList::orderBy('hotel_star_rating','asc')->distinct('hotel_star_rating')->pluck('hotel_star_rating'),
            'currenies' => Currency::all(),
        ];
        return view('supplier.pages.hotel.form', $data);

    }

    public function insert_newHotel(Request $request){

        $supplier_user = Auth::guard('supplier')->user();

        $hotel_code = SupplierHotelList::max('hotel_code');
        $hotel_code_num = str_pad($hotel_code + 1, 10, 0, STR_PAD_LEFT);
        $hotel_id = SupplierHotelList::max('hotel_code');
        $query = $request->all();
        $query['supplier_id'] = $supplier_user->id;
        $query['hotel_code'] = $hotel_code_num;
        $query['created_date'] = date('Y-m-d');

        $hotel_img = $request->uploadfile1;

        $supplier_hotel = new SupplierHotelList();
        // //step 1 form
        $supplier_hotel->supplier_id = $supplier_user->id;
        $supplier_hotel->hotel_code = $hotel_code_num;
        $supplier_hotel->admin_status=0;
        $supplier_hotel->created_date = date('Y-m-d');
        $supplier_hotel->hotel_name = $query['hotel_property_name'];
        $supplier_hotel->hotel_star_rating = $query['hotel_star_rating'];
        $supplier_hotel->hotel_property_type = $query['hotel_property_type'];
        $supplier_hotel->hotel_city = $query['hotel_city'];
        $supplier_hotel->hotel_country = $query['hotel_country'];
        $supplier_hotel->cityid = $query['cityid'];
        $supplier_hotel->address = $query['address'];
        $supplier_hotel->currency_type = $query['currency_type'];
        $supplier_hotel->government_tax = $query['government_tax'];
        $supplier_hotel->resort_fee = $query['resort_fee'];
        $supplier_hotel->service_tax = $query['service_tax'];

        $supplier_hotel->tax_included = $query['tax_include'];
        
        //step 2 form
        $supplier_hotel->location = $query['hotel_location'];
        $supplier_hotel->latitude = $query['hotel_Latitude'];
        $supplier_hotel->longitude  = $query['hotel_Longitude'];
        $supplier_hotel->hotel_email  = $query['hotel_email'];
        $supplier_hotel->reservation_email = $query['hotel_re_email'];
        $supplier_hotel->hotel_phone = $query['hotel_phone'];
        $supplier_hotel->hotel_fax = $query['hotel_fax'];
        $supplier_hotel->booking_phone = $query['booking_phone'];
        $supplier_hotel->emergency_no = $query['emergency_number'];
        $supplier_hotel->ta_location_id = $query['ta_locationId'];
        //step3 form
        $supplier_hotel->hotel_facilities =implode(',',$query['hotel_facilities']);
        //step4 form
        //images
        //step5 form
        $supplier_hotel->check_in = $query['check_in'];
        $supplier_hotel->check_in = $query['check_out'];
        $supplier_hotel->minimum_check_in = $query['min_check_in'];
        $supplier_hotel->Check_in_instructions = $query['check-in_instro'];
        $supplier_hotel->hotel_desc = $query['hotel_description'];
        $supplier_hotel->meal_plan_desc = $query['meal_plan_desc'];
        $supplier_hotel->imp_information = $query['important_information'];
        $supplier_hotel->language = implode(',',$query['language']);
        $supplier_hotel->close_by = implode(',',$query['able_restaurant']);
        $supplier_hotel->hotel_parking_type = $query['paking_Type'];
        $supplier_hotel->pets_allow = $query['allow_pets'];
        $supplier_hotel->nearby = implode(',',$query['nearby_things']);
        $supplier_hotel->entertainment = implode(',',$query['entertainment_facilities']);
        $supplier_hotel->pool = implode(',',$query['pool_spa']);
        $supplier_hotel->transfers = implode(',',$query['able_transfers']);
        $supplier_hotel->internet = $query['internet'];
        //step6 form
        $supplier_hotel->policy = $query['hotel_policy'];
        $supplier_hotel->child_policy = $query['child_policy'];
        $supplier_hotel->terms_and_condition = $query['term_condition'];
        $supplier_hotel->rate_desc = $query['rate_description'];
        $supplier_hotel->room_charge_disclosure = $query['room_charges'];
        
        $supplier_hotel->save();

        return response()->json([
    		"result" => "success",
			"message" => 'trus'
        ]);
        
    }
    //hotel citys
    public function get_HotelCity(Request $request){
        
        $query = $request->all();
        $query1 = $query['term'];
        $city_list = JamaicanCityList::where('city_name', 'like', '%'. $query['term'] .'%')->get();
        $citys = array();
        foreach ($city_list as $city_key) {
            $city_str =  $city_key->city_name . ', ' . $city_key->country_name;
            $citys[] = array(
                'label' => ucfirst($city_str),
                'value' => ucfirst($city_str),
                'id'=> $city_key->id,
                'city_name'=>ucfirst($city_key->city_name),
                'country_name'=>$city_key->country_name
            );
        }
        
        return response()->json($citys);;
    }
    //set hotel activation
    public function set_Acivation(Request $request){
        $hotel_object = SupplierHotelList::find($request->id);
        $hotel_object->status = $request->active_val;
        $hotel_object->save();
        return response()->json([
    		"result" => "success",
			"message" => 'success update'
        ]);
    }
    //user edit
    public function editUser()
    {
        return view('supplier.pages.page-users-edit');
    }
}
