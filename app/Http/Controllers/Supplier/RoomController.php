<?php 
namespace App\Http\Controllers\Supplier;

use App\Http\Controllers\Controller;
use App\Models\GlbHotelRoomType;
use App\Models\GlbHotelMealPlan;
use App\Models\GlbHotelFacilitiesType;
use App\Models\SupplierRoomList;
use App\Models\SupplierHotelList;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class RoomController extends Controller
{
    public function room_type(){
        $supplier_user = Auth::guard('supplier')->user();
        $pageConfigs = ['isContentSidebar' => true, 'bodyCustomClass' => 'todo-application'];

        $breadcrumbs = [
            ["link" => "/", "name" => "Home"],["name" => "Rooms"],["name" => "Room Type"]
        ];
        $data = [
            'pageConfigs'=>$pageConfigs,
            'breadcrumbs'=>$breadcrumbs,
            'room_type_list'=> $supplier_user->hotel_room_types,
        ];
        return view('supplier.pages.hotel.roomTypes', $data);
    }

    public function Add_NewRoom(){

        $supplier_user = Auth::guard('supplier')->user();
        $pageConfigs = ['pageHeader' => true];
    
        $breadcrumbs = [
          ["link" => "/", "name" => "Home"],["link" => "#", "name" => "Rooms"],["name" => "Add Room"]
        ];
        $facilities = GlbHotelFacilitiesType::all();
        $facilities = $facilities->where('facility_type', 'room');
        
        $data = [
            'pageConfigs'=>$pageConfigs,
            'breadcrumbs'=>$breadcrumbs,
            'hotels'=>$supplier_user->hotel_list,
            'room_types'=>$supplier_user->hotel_room_types,
            'mail_plan'=> $supplier_user->hotel_mealPan,
            'facilities'=> $facilities,
            'room_data'=>'',
        ];
        
        return view('supplier.pages.hotel.roomform',$data);
    }

    public function Room_Edit($id){

        $supplier_user = Auth::guard('supplier')->user();
        $room_data = SupplierRoomList::find($id);
        $pageConfigs = ['pageHeader' => true];
    
        $breadcrumbs = [
          ["link" => "/", "name" => "Home"],["link" => "#", "name" => "Rooms"],["name" => "Edit Room"]
        ];
        $facilities = GlbHotelFacilitiesType::all();
        $facilities = $facilities->where('facility_type', 'room');
        
        $data = [
            'pageConfigs'=>$pageConfigs,
            'breadcrumbs'=>$breadcrumbs,
            'hotels'=>SupplierHotelList::all(),
            'room_types'=>$supplier_user->hotel_room_types,
            'mail_plan'=> $supplier_user->hotel_mealPan,
            'facilities'=> $facilities,
            'room_data'=>$room_data,
        ];
        
        return view('supplier.pages.hotel.roomform',$data);

    }

    public function RoomRateCalendar(){
        $pageConfigs = ['bodyCustomClass' => 'calendar-application'];
        return view('supplier.pages.hotel.roomRatecalendar', ['pageConfigs' => $pageConfigs]);
        
    }

    public function insert_room(Request $request){
        $supplier_user = Auth::guard('supplier')->user();
        $query = $request->all();
        $room_code = SupplierRoomList::max('room_code');
        $room_code_num = str_pad($room_code + 1, 10, 0, STR_PAD_LEFT);
        $supplier_hotel = SupplierHotelList::all();
        $supplier_hotel_id = $supplier_hotel->where('hotel_code', $query['hotel_name']);
        
        
        $new_room = new SupplierRoomList();
        $new_room->supplier_id = $supplier_user->id; 
        $new_room->supplier_hotel_list_id = $supplier_hotel_id[0]->supplier_hotel_list_id; 
        $new_room->room_code = $room_code_num;
        $new_room->hotel_code = $query['hotel_name'];
        $new_room->room_name = $query['roomname'];
        $new_room->hotel_room_type = $query['room_type'];
        $new_room->room_desc = $query['room_detail'];
        $new_room->mealplan = implode(',',$query['mealplan']);
        $new_room->room_facilities = implode(',',$query['facilities']);
        $new_room->childageminlimit = 3;
        $new_room->childagemaxlimit = $query['child_max_age'];
        $new_room->minadult = $query['adults_min'];
        $new_room->maxadult = $query['adults_max'];
        $new_room->minchild = $query['childerns_min'];
        $new_room->maxchild = $query['childerns_max'];
        $new_room->minperson = $query['persons_min'];
        $new_room->maxperson = $query['persons_max'];
        $new_room->created_date = trim(date("Y-m-d"));
        $new_room->save();
        return redirect('supplier/roomList');
      }

    public function Room_List(){
        $supplier_user = Auth::guard('supplier')->user();
      
        $breadcrumbs = [
            ["link" => "/", "name" => "Home"],["name" => "Rooms"],["name" => "Room List"]
        ];
        $rooms_list = $supplier_user->hotel_Room_list;
        $hotels_lists = [];
        $supplier_hotel;
        $hotel_codes = [];
        foreach($rooms_list as $room_list){
            $supplier_hotel = SupplierHotelList::find($room_list->supplier_hotel_list_id);
            array_push($hotels_lists, $supplier_hotel);
            if( !in_array($room_list->hotel_code, $hotels_lists)){
                array_push($hotel_codes, $room_list->hotel_code);
            }
            
        }
        $data = [
            'breadcrumbs'=>$breadcrumbs,
            'roomlists'=>$rooms_list,
            'hotels_names'=>$hotels_lists,
            'hotel_codes'=>$hotel_codes,
        ];
        return view('supplier.pages.hotel.roomlist', $data);
    }

    public function updateRoomStatus($id){
        $room = SupplierRoomList::find($id);
        $room_status = $room->status;
        if($room_status == 1){
            $room->status = 0;
        }else{
            $room->status = 1;
        }
        $room->save();
        return response()->json([
            "result" => "success",
            "message" => $room->status,
            "status" =>0
            ]);

    }

    public function addNew_RoomType(Request $request){
        $query = $request->all();
        $supplier_user = Auth::guard('supplier')->user();
        $new_RoomType = new GlbHotelRoomType();
        $new_RoomType->room_type = $query['title'];
        $new_RoomType->supplier_id = $supplier_user->id;
        $new_RoomType->date_time =  date('Y-m-d H:i:s');
        $new_RoomType->created_date =  date('Y-m-d H:i:s');
        $new_RoomType->status = 0;
        $new_RoomType->save();
        return response()->json([
            "result" => "success",
            "message" => $new_RoomType->id,
            "status" =>0
            ]);

    }

    public function updateStatus_roomType($id, Request $request){
        $room_type = GlbHotelRoomType::find($id);
        $request_get = $request->all();
        if($request_get['active'] == 1){
            $room_type->status = 0;
        }else{
            $room_type->status = 1;
        }
        $room_type->save();
        return response()->json([
            "result" => "success",
            "message" => $room_type->status
            ]);
    }
    public function updateBlock_roomType($id){
        $room_type = GlbHotelRoomType::find($id);
        $room_type->status = 2;
        $room_type->save();
        return response()->json([
            "result" => "success",
            "message" => $room_type->status
            ]);
    }
}
?>